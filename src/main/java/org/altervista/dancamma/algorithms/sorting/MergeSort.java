package org.altervista.dancamma.algorithms.sorting;

import java.util.Arrays;

/**
 * MergeSort is a recursive algorithm for sorting.
 * <p/>
 * It works as following:
 * I split the array into two pieces, and call merge sort on them.
 * I expect to obtain two orderd halves.
 * Then, i merge the two halves by inserting in the final array elements from one half or the other (the lowest) until all elements are merged.
 * The base case is when I have only two elements, that I can swap if the first is bigger than the second.
 * <p/>
 * <p/>
 * In worst case it executes with O(nlogn)
 */
public class MergeSort {

    public static void mergeSort(int[] input) {
        mergeSort(input,new int[input.length],0,input.length-1);
    }

    private static void mergeSort(int[] input, int aux[], int start, int end) {
        if (start >= end) {return;}
        int middle = (end+start)/2;
        mergeSort(input,aux,start,middle);
        mergeSort(input,aux,middle+1,end);
        merge(input,aux,start,middle,end);
    }

    private static void merge(int[] input, int[] aux, int start, int middle, int end) {
        int i = start, j = middle+1, k = start;
        while (i<=middle && j <= end) {
            if(input[i]<input[j]) {
                aux [k++] = input[i++];
            } else {
                aux[k++] = input[j++];
            }
        }
        while (i <= middle) {
            aux [k++] = input[i++];
        }
        while (j <= end) {
            aux [k++] = input[j++];
        }
        for (k = start; k <= end; k++) {
            input[k]=aux[k];
        }
    }

    public static void mergeSort(char[] input) {
        mergeSort(input,new char[input.length],0,input.length-1);
    }

    private static void mergeSort(char[] input, char aux[], int start, int end) {
        if (start >= end) {return;}
        int middle = (end+start)/2;
        mergeSort(input,aux,start,middle);
        mergeSort(input,aux,middle+1,end);
        merge(input,aux,start,middle,end);
    }

    private static void merge(char[] input, char[] aux, int start, int middle, int end) {
        int i = start, j = middle+1, k = start;
        while (i<=middle && j <= end) {
            if(input[i]<input[j]) {
                aux [k++] = input[i++];
            } else {
                aux[k++] = input[j++];
            }
        }
        while (i <= middle) {
            aux [k++] = input[i++];
        }
        while (j <= end) {
            aux [k++] = input[j++];
        }
        for (k = start; k <= end; k++) {
            input[k]=aux[k];
        }
    }
}
