package org.altervista.dancamma.data_strctures;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Graph representation through an adjacency list
 */
public class Graph {

    //the adjacency list
    private List<Integer>[] adjacencyList;

    //number of vertices
    private int V;
    //number of edges
    private int E;

    //create a graph of n vertices
    public Graph(int n) {
        adjacencyList = new ArrayList[n];
        for (int i = 0; i < n; i++) {
            adjacencyList[i] = new ArrayList<Integer>();
        }
        V = n;
    }

    public void addEdge(int v, int w) {
        //what if overflow?
        adjacencyList[v].add(w);
        adjacencyList[w].add(v);
        E++;
    }

    public Iterable<Integer> getAdjacents(int v) {
        return adjacencyList[v];
    }

    public int getSize() {
        return V;
    }

}
