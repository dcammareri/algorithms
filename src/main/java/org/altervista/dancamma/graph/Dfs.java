package org.altervista.dancamma.graph;

import org.altervista.dancamma.data_strctures.Graph;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/**
 * Created by daniele on 29/05/14.
 */
public class Dfs {

    public static List<Integer> getConnected(Graph g, int v) {
        boolean[] marked = new boolean[g.getSize()];
        dfs(g, v, marked);
        List<Integer> result = new ArrayList<Integer>();
        for (int i = 0; i < marked.length; i++) {
            if (marked[i]) {
                result.add(i);
            }
        }
        return result;
    }

    private static void dfs(Graph g, int v, boolean[] marked) {
        if (marked[v]) {
            return;
        }
        marked[v] = true;
        Iterable<Integer> adjacents = g.getAdjacents(v);
        for (Integer adjacent : adjacents) {
            dfs(g, adjacent, marked);
        }
    }

    public static boolean hasPathTo(Graph g, int s, int v) {
        boolean[] marked = new boolean[g.getSize()];
        int[] edgeTo = new int[g.getSize()];
        dfsPathTo(g, s, marked, edgeTo);
        return marked[v];
    }

    public static Iterable<Integer> pathTo(Graph g, int s, int v) {
        boolean[] marked = new boolean[g.getSize()];
        int[] edgeTo = new int[g.getSize()];
        dfsPathTo(g, s, marked, edgeTo);

        if (!marked[v]) {
            return null;
        }
        Stack<Integer> path = new Stack<Integer>();
        for (int i = v; i != s; i = edgeTo[i]) {
            path.push(i);
        }
        path.push(s);
        return path;


    }

    private static void dfsPathTo(Graph g, int s, boolean[] marked, int[] edgeTo) {
        marked[s] = true;
        for (Integer adjacent : g.getAdjacents(s)) {
            if (!marked[adjacent]) {
                edgeTo[adjacent] = s;
                dfsPathTo(g, adjacent, marked, edgeTo);
            }
        }
    }

}

