package org.altervista.dancamma.careercup.exercise;

import org.altervista.dancamma.algorithms.sorting.MergeSort;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by daniele on 08/05/14.
 */
public class OneDotTwo {

    public static void reverse (char[] s) {
        int left = 0;
        int right = s.length - 2;
        char aux = 0;
        while (left < right) {
            swap(s,left++,right--,aux);
        }
        char[] a  = null;
    }

    public static void swap (char []s, int left, int right, char aux) {
        aux = s[left];
        s[left] = s[right];
        s[right] = aux;
    }
}
