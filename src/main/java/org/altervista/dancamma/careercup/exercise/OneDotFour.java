package org.altervista.dancamma.careercup.exercise;

import org.altervista.dancamma.algorithms.sorting.MergeSort;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by daniele on 12/05/14.
 */
public class OneDotFour {

    public static boolean isAnagram(char[] s1, char[] s2) {

        if (s1.length != s2.length) {
            return false;
        }

        Map<Character, Integer> chars = new HashMap<Character, Integer>();
        Integer value;
        for (char c : s1) {
            value = chars.get(c);
            if (value == null) {
                chars.put(c, 1);
            } else {
                chars.put(c, ++value);
            }

        }

        for (char c : s2) {
            value = chars.get(c);
            if (value == null) {
                return false;
            } else {
                if (--value < 0) {
                    return false;
                } else {
                    chars.put(c, value);
                }
            }
        }

        for (Integer val : chars.values()) {
            if (val != 0) {
                return false;
            }
        }

        return true;

    }

    public static boolean isAnagram2(char[] s1, char[] s2) {
        if (s1.length != s2.length) {
            return false;
        }
        MergeSort.mergeSort(s1);
        MergeSort.mergeSort(s2);

        for (int i = 0; i < s1.length; i++) {
            if (s1[i] != s2[i]) {
                return false;
            }
        }
        return true;
    }

    public static boolean isAnagramBook(String s, String t) {

        if (s.length() != t.length()) return false;

        int[] letters = new int[256];

        int num_unique_chars = 0;

        int num_completed_t = 0;

        char[] s_array = s.toCharArray();

        for (char c : s_array) { // count number of each char in s.

            if (letters[c] == 0) ++num_unique_chars;

            ++letters[c];

        }

        for (int i = 0; i < t.length(); ++i) {

            int c = (int) t.charAt(i);

            if (letters[c] == 0) { // Found more of char c in t than in s.

                return false;

            }

            --letters[c];

            if (letters[c] == 0) {

                ++num_completed_t;

                if (num_completed_t == num_unique_chars) {

// it’s a match if t has been processed completely

                    return i == t.length() - 1;

                }

            }

        }

        return false;
    }

}
