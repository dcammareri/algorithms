package org.altervista.dancamma.careercup.exercise;

import org.altervista.dancamma.data_strctures.Node;

/**
 * Created by daniele on 18/05/14.
 */
public class FourDotOne {

    public static boolean isBalanced(Node n) {
        if (n == null) {
            return false;
        }
        int minDepth = minDepth(n);
        int maxDepth = maxDepth(n);

        return maxDepth-minDepth <= 1;
    }

    private static int minDepth (Node n) {
        if (n.getLeft() == null || n.getRight() == null) {return 0;}
        int minLeft = 0;
        int minRight = 0;
        if (n.getLeft()!=null) {
            minLeft = minDepth(n.getLeft());
        }
        if (n.getRight()!=null) {
            minRight = minDepth(n.getRight());
        }
        return 1+Math.min(minLeft,minRight);
    }

    private static int maxDepth (Node n) {
        if (n.getLeft() == null && n.getRight() == null) {return 0;}
        int minLeft = 0;
        int minRight = 0;
        if (n.getLeft()!=null) {
            minLeft = maxDepth(n.getLeft());
        }
        if (n.getRight()!=null) {
            minRight = maxDepth(n.getRight());
        }
        return 1+Math.max(minLeft,minRight);
    }

}
