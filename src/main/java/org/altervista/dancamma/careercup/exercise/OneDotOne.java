package org.altervista.dancamma.careercup.exercise;

import org.altervista.dancamma.algorithms.sorting.MergeSort;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by daniele on 08/05/14.
 */
public class OneDotOne {

    public static boolean isUnique (String input) {
        Set<Character> chars = new HashSet<Character>();
        for (char c : input.toCharArray()) {
            if (chars.contains(c)) {
                return false;
            } else {
                chars.add(c);
            }
        }
        return true;
    }

    public static boolean isUniqueWithArray(String input) {
        boolean[] chars = new boolean[256]; //if ASCII
        for (char c : input.toCharArray()) {
            if (chars[c]) { return false; }
            chars[c] = true;
        }
        return true;
    }

    public static boolean isUniqueWithoutHash (String input) {
        char[] ordered = input.toCharArray();
        MergeSort.mergeSort(ordered);
        for (int i = 0; i < ordered.length-1;i++) {
            if (ordered[i]==ordered[i+1]) {
                return false;
            }
        }
        return true;
    }
}
