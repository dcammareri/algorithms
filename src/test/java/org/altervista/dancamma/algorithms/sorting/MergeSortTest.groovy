package org.altervista.dancamma.algorithms.sorting

import spock.lang.Specification
import spock.lang.Unroll

/**
 * Created by daniele on 09/05/14.
 */
class MergeSortTest extends Specification {
    @Unroll("#a sorted is #b")
    def "MergeSort"() {
        when:
        MergeSort.mergeSort(a)

        then:
        a == b

        where:
        a                        | b
        [1, 2, 3] as int[]       | [1, 2, 3] as int[]
        [1, 3, 2] as int[]       | [1, 2, 3] as int[]
        [2, 1, 3] as int[]       | [1, 2, 3] as int[]
        [2, 3, 1] as int[]       | [1, 2, 3] as int[]
        [3, 2, 1] as int[]       | [1, 2, 3] as int[]
        [3, 1, 2] as int[]       | [1, 2, 3] as int[]
        [4, 3, 2, 1] as int[]    | [1, 2, 3, 4] as int[]
        [4, 3, 2, 1] as int[]    | [1, 2, 3, 4] as int[]
        [1, 3, 4, 2, 5] as int[]    | [1, 2, 3, 4, 5] as int[]
        [1, 3, 4, 2, 1, 5] as int[]    | [1, 1, 2, 3, 4, 5] as int[]
        [1] as int[]             | [1] as int[]
        [] as int[]              | [] as int[]
        [-1, -2, -3, 2] as int[] | [-3, -2, -1, 2] as int[]

    }

}
