package org.altervista.dancamma.graph

import org.altervista.dancamma.data_strctures.Graph
import org.altervista.dancamma.graph.Dfs
import spock.lang.Specification

/**
 * Created by daniele on 29/05/14.
 */
class DfsTest extends Specification {
    def "GetConnected"() {
        given:
        Graph g = new Graph(7)
        g.addEdge 0, 5
        g.addEdge 2, 4
        g.addEdge 2, 3
        g.addEdge 1, 2
        g.addEdge 0, 1
        g.addEdge 3, 4
        g.addEdge 3 ,5
        g.addEdge 0, 2

        expect:
        Dfs.getConnected(g,0) == [0,1,2,3,4,5]
    }

    def "pathTo"() {
        given:
        Graph g = new Graph(7)
        g.addEdge 0, 5
        g.addEdge 2, 4
        g.addEdge 2, 3
        g.addEdge 1, 2
        g.addEdge 0, 1
        g.addEdge 3, 4
        g.addEdge 3 ,5
        g.addEdge 0, 2
        g.addEdge 5, 6

        expect:
        println Dfs.pathTo(g,0,a)

        where:
        a << [0,1,2,3,4,5,6]



    }
}
