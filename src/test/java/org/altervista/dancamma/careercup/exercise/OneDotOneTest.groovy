package org.altervista.dancamma.careercup.exercise

/**
 * Created by daniele on 08/05/14.
 */
class OneDotOneTest extends spock.lang.Specification {
    def "IsUnique"() {
        expect:
        OneDotOne.isUnique(a) == b
        OneDotOne.isUniqueWithArray(a) == b
        OneDotOne.isUniqueWithoutHash(a) == b
        where:
        a|b
        ''|true
        'ab' | true
        'aa' | false
        'aba' | false

    }

}
