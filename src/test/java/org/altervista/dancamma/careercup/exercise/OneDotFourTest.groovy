package org.altervista.dancamma.careercup.exercise

import spock.lang.Specification

/**
 * Created by daniele on 12/05/14.
 */
class OneDotFourTest extends Specification {
    def "IsAnagram"() {
        expect:
        OneDotFour.isAnagram(a.toCharArray(), b.toCharArray()) == c
        OneDotFour.isAnagram2(a.toCharArray(), b.toCharArray()) == c
        OneDotFour.isAnagramBook(a,b) == c

        where:
        a      | b      | c
        ""     | ""     | true
        "a"    | "a"    | true
        "a"    | "b"    | false
        "ab"   | "ba"   | true
        "aab"  | "ab"   | false
        "ab"   | "aab"  | false
        "abab" | "baba" | true

    }
}
