package org.altervista.dancamma.careercup.exercise

import spock.lang.Specification

/**
 * Created by daniele on 11/05/14.
 */
class OneDotTwoTest extends Specification {
    def "Reverse"() {
        when:
        OneDotTwo.reverse(a)

        then:
        a==b

        where:
        a|b
        [] as char[] | [] as char[]
        ['\0'] as char[] | ['\0'] as char[]
        ['a','\0'] as char[] | ['a','\0'] as char[]
        ['a', 'b', '\0'] as char[] | ['b','a', '\0'] as char[]
        ['a', 'b', 'c', '\0'] as char[] | ['c','b', 'a', '\0'] as char[]

    }
}
