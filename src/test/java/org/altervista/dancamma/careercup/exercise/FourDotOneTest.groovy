package org.altervista.dancamma.careercup.exercise

import spock.lang.Specification
import org.altervista.dancamma.data_strctures.Node
/**
 * Created by daniele on 18/05/14.
 */
class FourDotOneTest extends Specification {

    def "isBalanced"() {
        expect:
        FourDotOne.isBalanced(a) == b

        where:
        a                                                                             | b
        null                                                                          | false
        new Node()                                                                    | true
        new Node(left: new Node())                                                    | true
        new Node(left: new Node(left: new Node()))                                    | false
        new Node(left: new Node(left: new Node()), right: new Node())                 | true
        new Node(left: new Node(left: new Node(left: new Node())), right: new Node()) | false

    }
}
